/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 88.84848484848484, "KoPercent": 11.151515151515152};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.3325925925925926, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.3933333333333333, 500, 1500, "/api/products - Post Product"], "isController": false}, {"data": [0.14666666666666667, 500, 1500, "Orders"], "isController": true}, {"data": [0.0, 500, 1500, "Products"], "isController": true}, {"data": [0.3375, 500, 1500, "/api/products/id/ratings - Assign Product Rating"], "isController": false}, {"data": [0.16833333333333333, 500, 1500, "/api/products/id- Get Product by ID"], "isController": false}, {"data": [0.5716666666666667, 500, 1500, "/api/auth/info - Invalid Token"], "isController": false}, {"data": [0.5233333333333333, 500, 1500, "/api/auth/login - Login"], "isController": false}, {"data": [0.0, 500, 1500, "/api/products - Get All Products"], "isController": false}, {"data": [0.49666666666666665, 500, 1500, "Hello"], "isController": true}, {"data": [0.0033333333333333335, 500, 1500, "/api/auth/register - Register"], "isController": false}, {"data": [0.4866666666666667, 500, 1500, "/api/categories/id - Get Category by ID"], "isController": false}, {"data": [0.49333333333333335, 500, 1500, "/api/categories - Post Categories"], "isController": false}, {"data": [0.375, 500, 1500, "/api/products/id/comments - Get Product Comment"], "isController": false}, {"data": [0.4666666666666667, 500, 1500, "/api/orders - Get All Orders"], "isController": false}, {"data": [0.035, 500, 1500, "Categories"], "isController": true}, {"data": [0.4083333333333333, 500, 1500, "/api/products/id - Delete Product"], "isController": false}, {"data": [0.4216666666666667, 500, 1500, "/api/orders - Post New Order"], "isController": false}, {"data": [0.395, 500, 1500, "/api/categories - Get All Categories"], "isController": false}, {"data": [0.12166666666666667, 500, 1500, "/api/auth/register - Same Email Register"], "isController": false}, {"data": [0.49666666666666665, 500, 1500, "/api/hello - Hello"], "isController": false}, {"data": [0.6033333333333334, 500, 1500, "/api/auth/login - Invalid Login"], "isController": false}, {"data": [0.37166666666666665, 500, 1500, "/api/categories/id- Delete Category by ID"], "isController": false}, {"data": [0.0, 500, 1500, "Authentication"], "isController": true}, {"data": [0.49833333333333335, 500, 1500, "/api/orders/id - Get Order by ID"], "isController": false}, {"data": [0.48, 500, 1500, "/api/auth/info - Get User Information"], "isController": false}, {"data": [0.34833333333333333, 500, 1500, "/api/products/id/comments - Post Product Comment"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 6600, 736, 11.151515151515152, 10618.240909090926, 23, 65640, 1535.0, 40053.40000000001, 60051.0, 60455.97, 24.58796530861622, 48.24100292354633, 9.840930737145339], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["/api/products - Post Product", 300, 27, 9.0, 15684.530000000012, 60, 60922, 1542.0, 59732.000000000015, 60097.45, 60383.64, 1.1814605214966742, 0.6234242577966549, 0.6298584487226443], "isController": false}, {"data": ["Orders", 300, 40, 13.333333333333334, 19942.469999999998, 93, 109275, 12147.0, 50279.3, 64857.549999999996, 106079.79000000014, 1.518164842338581, 1.5433341572540447, 2.011380622270466], "isController": true}, {"data": ["Products", 300, 155, 51.666666666666664, 117104.96999999997, 7266, 240663, 112138.5, 185652.60000000003, 196473.34999999995, 232376.2, 1.156925355176084, 23.97434801230583, 4.051773283363799], "isController": true}, {"data": ["/api/products/id/ratings - Assign Product Rating", 600, 108, 18.0, 11794.754999999996, 30, 63227, 1650.0, 41248.0, 60038.6, 60917.96, 2.528892597931366, 1.0338330267051057, 1.1033240646785356], "isController": false}, {"data": ["/api/products/id- Get Product by ID", 300, 44, 14.666666666666666, 18572.053333333348, 55, 61249, 5498.5, 60117.5, 60299.45, 60613.83, 1.2319721410033182, 2.1958900831889188, 0.4374784404875324], "isController": false}, {"data": ["/api/auth/info - Invalid Token", 300, 19, 6.333333333333333, 8741.680000000002, 28, 60422, 583.5, 39203.0, 60064.6, 60257.95, 1.5888568175197946, 0.6511882083653312, 0.3560704674549162], "isController": false}, {"data": ["/api/auth/login - Login", 300, 29, 9.666666666666666, 3779.4500000000003, 40, 60505, 576.5, 3851.1000000000013, 25570.099999999973, 60375.0, 2.381330369899984, 1.1411316528417208, 0.8376980721146213], "isController": false}, {"data": ["/api/products - Get All Products", 300, 38, 12.666666666666666, 29195.676666666688, 5035, 62897, 18892.0, 60067.0, 60242.5, 60885.48, 1.1593037994249853, 19.044010312007295, 0.41485490226102884], "isController": false}, {"data": ["Hello", 300, 5, 1.6666666666666667, 7367.086666666677, 23, 60269, 1108.0, 20706.0, 37638.49999999987, 60050.99, 1.2727990123079664, 0.38759050131310435, 0.2955611664778682], "isController": true}, {"data": ["/api/auth/register - Register", 300, 19, 6.333333333333333, 3676.9900000000002, 621, 65640, 2668.0, 5523.100000000001, 6002.249999999999, 11396.170000000004, 4.569966182250251, 2.432406153840295, 1.7347871300231545], "isController": false}, {"data": ["/api/categories/id - Get Category by ID", 300, 25, 8.333333333333334, 13416.156666666662, 47, 61012, 748.5, 54975.20000000006, 60150.25, 60581.6, 1.288532490346744, 2.610343681144045, 0.49285948311592925], "isController": false}, {"data": ["/api/categories - Post Categories", 300, 29, 9.666666666666666, 11695.63666666667, 39, 60923, 703.5, 57480.400000000314, 60167.95, 60909.01, 1.4351800911817751, 0.6973844665076806, 0.6639343055283138], "isController": false}, {"data": ["/api/products/id/comments - Get Product Comment", 300, 39, 13.0, 9899.32666666667, 29, 60640, 1576.0, 24257.400000000038, 42723.75, 60269.37, 1.2709925604568795, 0.5219261442110525, 0.5164896721262858], "isController": false}, {"data": ["/api/orders - Get All Orders", 300, 32, 10.666666666666666, 6903.800000000002, 31, 60907, 568.5, 20570.7, 38994.54999999999, 60113.26, 1.6906266025731338, 0.5010792713681114, 0.6840323663418785], "isController": false}, {"data": ["Categories", 300, 97, 32.333333333333336, 54073.80999999997, 358, 181894, 44368.5, 121400.1, 143710.94999999998, 172448.33000000002, 1.1829139903237635, 21.850161886708385, 1.965412365049229], "isController": true}, {"data": ["/api/products/id - Delete Product", 300, 36, 12.0, 8456.640000000001, 41, 61341, 1259.0, 22448.40000000001, 40633.3, 61140.47000000001, 1.271806176738665, 0.42498695073870746, 0.6171571952434449], "isController": false}, {"data": ["/api/orders - Post New Order", 300, 35, 11.666666666666666, 7913.860000000005, 26, 61234, 931.5, 21199.9, 40141.399999999994, 60259.78, 1.5189335061542122, 0.6527211298207152, 0.7796450331507238], "isController": false}, {"data": ["/api/categories - Get All Categories", 300, 32, 10.666666666666666, 14540.76666666667, 102, 61313, 1376.5, 60034.5, 60160.7, 60887.94, 1.3000351009477256, 20.136896235260853, 0.4804374509778431], "isController": false}, {"data": ["/api/auth/register - Same Email Register", 300, 8, 2.6666666666666665, 5296.409999999997, 67, 62815, 3068.5, 4339.900000000001, 27871.2999999999, 62703.070000000014, 2.7018994353030177, 1.1192565639269407, 1.0596247990462295], "isController": false}, {"data": ["/api/hello - Hello", 300, 5, 1.6666666666666667, 7367.086666666674, 23, 60269, 1108.0, 20706.0, 37638.49999999987, 60050.99, 1.2727990123079664, 0.38759050131310435, 0.2955611664778682], "isController": false}, {"data": ["/api/auth/login - Invalid Login", 300, 11, 3.6666666666666665, 5343.033333333332, 27, 60487, 526.5, 12757.40000000007, 43624.549999999996, 60313.36, 1.6162138574176135, 0.608558180668466, 0.5556103412770245], "isController": false}, {"data": ["/api/categories/id- Delete Category by ID", 300, 60, 20.0, 14421.249999999996, 33, 61412, 1386.0, 60038.9, 60149.7, 60852.420000000006, 1.1913176767716878, 0.5601830112142704, 0.5323196729733701], "isController": false}, {"data": ["Authentication", 300, 60, 20.0, 35112.96333333333, 5551, 194673, 9991.5, 112972.00000000001, 149188.39999999982, 194336.43, 1.5360353902553914, 4.068098728994716, 3.2016487659875685], "isController": true}, {"data": ["/api/orders/id - Get Order by ID", 300, 31, 10.333333333333334, 5124.8099999999995, 29, 60307, 472.5, 19902.90000000001, 22457.899999999994, 47448.36000000003, 1.6951642614169313, 0.4923922440584493, 0.6899130928130687], "isController": false}, {"data": ["/api/auth/info - Get User Information", 300, 41, 13.666666666666666, 8275.400000000001, 46, 60921, 660.0, 40448.20000000001, 58405.59999999985, 60600.46, 1.5893029317341414, 0.6934989563577415, 0.6244687837726873], "isController": false}, {"data": ["/api/products/id/comments - Post Product Comment", 300, 68, 22.666666666666668, 11707.233333333334, 32, 60533, 1484.5, 41256.0, 60030.65, 60172.85, 1.269293257514216, 0.5367143735826225, 0.624387334621666], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["400/Bad Request", 71, 9.646739130434783, 1.0757575757575757], "isController": false}, {"data": ["Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 371, 50.40760869565217, 5.621212121212121], "isController": false}, {"data": ["502/Bad Gateway", 2, 0.2717391304347826, 0.030303030303030304], "isController": false}, {"data": ["405/Method Not Allowed", 108, 14.673913043478262, 1.6363636363636365], "isController": false}, {"data": ["401/Unauthorized", 166, 22.554347826086957, 2.515151515151515], "isController": false}, {"data": ["Non HTTP response code: javax.net.ssl.SSLHandshakeException/Non HTTP response message: Remote host terminated the handshake", 18, 2.4456521739130435, 0.2727272727272727], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 6600, 736, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 371, "401/Unauthorized", 166, "405/Method Not Allowed", 108, "400/Bad Request", 71, "Non HTTP response code: javax.net.ssl.SSLHandshakeException/Non HTTP response message: Remote host terminated the handshake", 18], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["/api/products - Post Product", 300, 27, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 27, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["/api/products/id/ratings - Assign Product Rating", 600, 108, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 30, "400/Bad Request", 26, "405/Method Not Allowed", 26, "401/Unauthorized", 26, "", ""], "isController": false}, {"data": ["/api/products/id- Get Product by ID", 300, 44, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 44, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["/api/auth/info - Invalid Token", 300, 19, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 19, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["/api/auth/login - Login", 300, 29, "400/Bad Request", 19, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 10, "", "", "", "", "", ""], "isController": false}, {"data": ["/api/products - Get All Products", 300, 38, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 38, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["/api/auth/register - Register", 300, 19, "Non HTTP response code: javax.net.ssl.SSLHandshakeException/Non HTTP response message: Remote host terminated the handshake", 18, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 1, "", "", "", "", "", ""], "isController": false}, {"data": ["/api/categories/id - Get Category by ID", 300, 25, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 25, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["/api/categories - Post Categories", 300, 29, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 29, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["/api/products/id/comments - Get Product Comment", 300, 39, "400/Bad Request", 26, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 13, "", "", "", "", "", ""], "isController": false}, {"data": ["/api/orders - Get All Orders", 300, 32, "401/Unauthorized", 29, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 3, "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["/api/products/id - Delete Product", 300, 36, "405/Method Not Allowed", 27, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 9, "", "", "", "", "", ""], "isController": false}, {"data": ["/api/orders - Post New Order", 300, 35, "401/Unauthorized", 29, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 6, "", "", "", "", "", ""], "isController": false}, {"data": ["/api/categories - Get All Categories", 300, 32, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 31, "502/Bad Gateway", 1, "", "", "", "", "", ""], "isController": false}, {"data": ["/api/auth/register - Same Email Register", 300, 8, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 8, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["/api/hello - Hello", 300, 5, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 4, "502/Bad Gateway", 1, "", "", "", "", "", ""], "isController": false}, {"data": ["/api/auth/login - Invalid Login", 300, 11, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 11, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["/api/categories/id- Delete Category by ID", 300, 60, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 32, "405/Method Not Allowed", 28, "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["/api/orders/id - Get Order by ID", 300, 31, "401/Unauthorized", 29, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 2, "", "", "", "", "", ""], "isController": false}, {"data": ["/api/auth/info - Get User Information", 300, 41, "401/Unauthorized", 27, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 14, "", "", "", "", "", ""], "isController": false}, {"data": ["/api/products/id/comments - Post Product Comment", 300, 68, "405/Method Not Allowed", 27, "401/Unauthorized", 26, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: altashop-api.fly.dev:443 failed to respond", 15, "", "", "", ""], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
